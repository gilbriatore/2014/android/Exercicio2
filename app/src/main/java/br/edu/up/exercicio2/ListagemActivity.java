package br.edu.up.exercicio2;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListagemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        Intent intent = getIntent();
        String str = intent.getExtras().getString("txt");
        String[] itens = ordenarStrings(str.split(","));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.simple_list_item_1, itens);
        ListView lstView = (ListView) findViewById(R.id.meuListView);
        lstView.setAdapter(adapter);
        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {
                Toast.makeText(ListagemActivity.this, "Teste", Toast.LENGTH_LONG).show();
            }
        });
    }

    @NonNull
    private String[] ordenarStrings(String[] itens) {
        String aux;
        for(int i = 0; i < itens.length - 1; i++) {
            for(int j = 0; j < itens.length - 1; j++) {
                if(itens[j].trim().compareTo(itens[j + 1].trim()) > 0) {
                    aux = itens[j].trim();
                    itens[j] = itens[j + 1].trim();
                    itens[j + 1] = aux;
                }
            }
        }
        return itens;
    }
}
