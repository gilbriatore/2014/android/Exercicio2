package br.edu.up.exercicio2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void EnviarTexto(View view) {

        EditText txt = (EditText) findViewById(R.id.txtLista);
        Intent intent = new Intent(this, ListagemActivity.class);
        intent.putExtra("txt", txt.getText().toString());
        startActivity(intent);

    }
}
